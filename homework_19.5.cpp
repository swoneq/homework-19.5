﻿#include <iostream>

class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "" << "\n";
	} 
};
 
class Dog : public Animal
{
public:
	void Voice()
	{
		std::cout << "Dog: Woof!" << std::endl;
	}
};

class Cat : public Animal
{
public:
	void Voice()
	{
		std::cout << "Cat: Meow!" << "\n";
	}
};
class Cow : public Animal
{
public:
	void Voice()
	{
		std::cout << "Cow: Mooo!" << "\n";
	}
};

int main()
{
	Dog Ram;
	Cat Martin;
	Cow Milka;

	Animal* zoo[3] = { &Ram,&Martin,&Milka };
	for (int i=0; i<1; i++)
	{
		zoo[0]->Voice();
		zoo[1]->Voice();
		zoo[2]->Voice();
	}
}
